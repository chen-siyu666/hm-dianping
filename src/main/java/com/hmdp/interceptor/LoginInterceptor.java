package com.hmdp.interceptor;


import com.hmdp.utils.UserHolder;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LoginInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.判断Threadlocal里面是否包含用户信息
//        request.setAttribute();
        if (UserHolder.getUser() == null) {
            //Threadlocal内未取到用户信息：拦截
            response.setStatus(401);
            return false;

        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserHolder.removeUser();
    }
}
