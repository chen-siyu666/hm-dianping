package com.hmdp.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import org.springframework.context.annotation.Configuration;


/**
 * redisson分布式锁的配置类
 */
@Configuration
public class RedissonConfig {

    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.152.100:6379").setPassword("atguigu");
        return Redisson.create(config);
    }
}
