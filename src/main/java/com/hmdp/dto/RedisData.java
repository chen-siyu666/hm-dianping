package com.hmdp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class RedisData {

    private Object data;
    private LocalDateTime localDateTime;
}
