package com.hmdp.dto;

import lombok.Data;

/**
 * 这个类用于传输用户的登录信息表单
 */
@Data
public class LoginFormDTO {
    private String phone;
    private String code;
    private String password;
}
