package com.hmdp.controller;


import com.hmdp.dto.Result;
import com.hmdp.service.IFollowService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@RestController
@RequestMapping("/follow")
public class FollowController {
    @Resource
    private IFollowService followService;

    @PutMapping("/{id}/{istrue}")
    public Result follow(@PathVariable("id") Long id,
                         @PathVariable("istrue") Boolean istrue) {
        return followService.follow(id, istrue);
    }

    @GetMapping("/or/not/{id}")
    public Result checkFollow(@PathVariable("id") Long id) {
        return followService.checkFollow(id);
    }

    @GetMapping("/common/{id}")
    public Result commonFollow(@PathVariable("id") Long id) {
        return followService.commonFollow(id);
    }
}
