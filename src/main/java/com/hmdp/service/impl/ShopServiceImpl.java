package com.hmdp.service.impl;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.RedisData;
import com.hmdp.dto.Result;
import com.hmdp.entity.Shop;
import com.hmdp.mapper.ShopMapper;
import com.hmdp.service.IShopService;
import com.hmdp.utils.SystemConstants;
import lombok.NonNull;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.domain.geo.GeoReference;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IShopService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private ShopMapper shopMapper;

    @Override
    public Result queryById(Long id) {
        //缓存穿透
        Shop shop = queryWithPassThough(id);
        return Result.ok(shop);
        //缓存击穿
//        Shop shop = null;
//        try {
//            shop = queryWithMutex(id);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        if (shop == null){
//            return Result.fail("查询的店铺不存在");
//        }
//        //8.返回商铺信息
//        return Result.ok(shop);
        //缓存击穿：逻辑过期(热点KEY问题)
//        Shop shop = null;
//        try {
//            shop = queryWithLogicalExpire(id);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        return Result.ok(shop);
    }


    public Shop queryWithMutex(Long id) throws InterruptedException {
        String KEY = CACHE_SHOP_KEY + id.toString();
        //1.从redis查询商铺的缓存
        String shopJSON = stringRedisTemplate.opsForValue().get(KEY);
        //2.判断缓存是否命中
        if (StrUtil.isNotBlank(shopJSON)) {
            //3.命中：返回商铺信息(将JSON转化为实体对象)
            Shop shop = JSONUtil.toBean(shopJSON, Shop.class, false);
            return shop;
        }

        if (shopJSON != null) {
            return null;
        }
        //尝试获取互斥锁
        Boolean lock = tryLock(id);
        //判断是否获取互斥锁
        if (!lock) {
            //获取失败：休眠一段时间继续从头开始执行
            Thread.sleep(50);
            return queryWithMutex(id);

        }
        //获取成功：在数据库中查询数据(二次判断：在获取锁后判断缓存内是否有数据)
        if (StrUtil.isNotBlank(shopJSON)) {
            //3.命中：返回商铺信息(将JSON转化为实体对象)
            Shop shop = JSONUtil.toBean(shopJSON, Shop.class, false);
            return shop;
        }
        //4.未命中：根据id到数据库中进行查询
        Shop shop = getById(id);        //getById方法查询一个对象，queryById方法查询的是Result类的对象
        //模拟重建的延时
        Thread.sleep(200);
        //5.判断数据库中是否能查询到该商铺
        if (shop == null) {
            //将空字符串缓存到redis中（防止缓存穿透）
            stringRedisTemplate.opsForValue().set(KEY, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
            //6.不能查询到：报错：返回404
            return null;
        }
        //7.可以查询到：将信息添加到redis中
        String jsonStr = JSONUtil.toJsonStr(shop);
        stringRedisTemplate.opsForValue().set(KEY, jsonStr, CACHE_SHOP_TTL, TimeUnit.MINUTES);

        unLock(id);
        //8.返回商铺信息
        return shop;
    }


    //创建线程池
    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

    /**
     * 基于逻辑过期解决缓存击穿问题
     *
     * @param id
     * @return
     * @throws InterruptedException
     */
    public Shop queryWithLogicalExpire(Long id) throws InterruptedException {
        String KEY = CACHE_SHOP_KEY + id.toString();
        //1.从redis查询商铺的缓存
        String shopJSON = stringRedisTemplate.opsForValue().get(KEY);
        //2.判断缓存是否命中
//        if (StrUtil.isBlank(shopJSON)) {
//            //2.1未命中：直接返回null
//            return null;
//        }

        //3.命中：判断缓存是否过期
        RedisData redisData = JSONUtil.toBean(shopJSON, RedisData.class);
        Object data = redisData.getData();
        Shop shop = JSONUtil.toBean((JSONObject) data, Shop.class);
        if (redisData.getLocalDateTime().isAfter(LocalDateTime.now())) {
            //3.1未过期：直接返回商铺信息
            return shop;
        }

        //3.2过期：进行缓存重建
        //4.基于缓存过期，尝试获取互斥锁
        Boolean lock = tryLock(id);
        //4.1获取互斥锁失败
        if (!lock) {
            //获取失败：直接返回旧数据
            //4.2返回过期数据
            return shop;
        }
        //4.3获取互斥锁成功：开启独立线程进行缓存重建
        //进行二次判断：在获取锁后判断缓存内是否过期（如果没有过期，代表其他线程已经更新完数据）
        if (redisData.getLocalDateTime().isAfter(LocalDateTime.now())) {
            return shop;
        }
        //5.开启独立线程
        CACHE_REBUILD_EXECUTOR.submit(() -> {
            try {
                //5.1根据id查询数据库
                //5.2将商铺信息写入redis，并设置逻辑过期时间
                this.saveDataToRedis(id, 20L);
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                //5.3释放互斥锁
                unLock(id);
            }
        });

        //6.返回商铺信息（新的信息）
        return shop;
    }


    public void saveDataToRedis(Long id, Long exprireTime) throws InterruptedException {
        //1.查询店铺信息
        Shop shop = queryBase(id);
        Thread.sleep(200);
        //2.将查出的数据封装成RedisData对象
        RedisData redisData = new RedisData();
        redisData.setData(shop);
        redisData.setLocalDateTime(LocalDateTime.now().plusSeconds(exprireTime));
        //3.将RedisData对象传入Redis中
        String jsonStr = JSONUtil.toJsonStr(redisData);
        stringRedisTemplate.opsForValue().set(CACHE_SHOP_KEY + id, jsonStr);
    }

    /**
     * 缓存穿透
     *
     * @param id
     * @return
     */
    public Shop queryWithPassThough(Long id) {
        String KEY = CACHE_SHOP_KEY + id.toString();
        //1.从redis查询商铺的缓存
        String shopJSON = stringRedisTemplate.opsForValue().get(KEY);
        //2.判断缓存是否命中
        if (StrUtil.isNotBlank(shopJSON)) {
            //3.命中：返回商铺信息(将JSON转化为实体对象)
//            Shop shop = JSONUtil.toBean(shopJSON, Shop.class, false);
            Shop shop = JSON.parseObject(shopJSON, Shop.class);
            return shop;
        }
        //判断命中的是否是空值（redis缓存的空字符串）
        //上面判断完有值的情况，在下面只可能有“ ” 或者 null的情况
        if (shopJSON != null) {
            return null;
        }
        //4.未命中：根据id到数据库中进行查询
        Shop shop = getById(id);        //getById方法查询一个对象，queryById方法查询的是Result类的对象
        //5.判断数据库中是否能查询到该商铺
        if (shop == null) {
            //将空字符串缓存到redis中（防止缓存穿透）
            stringRedisTemplate.opsForValue().set(KEY, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
            //6.不能查询到：报错：返回404
            return null;
        }
        //7.可以查询到：将信息添加到redis中
        String jsonStr = JSONUtil.toJsonStr(shop);
        stringRedisTemplate.opsForValue().set(KEY, jsonStr, CACHE_SHOP_TTL, TimeUnit.MINUTES);
        //8.返回商铺信息
        return shop;
    }


    public Shop queryBase(Long id) {
        String KEY = CACHE_SHOP_KEY + id.toString();

        //4.未命中：根据id到数据库中进行查询
        Shop shop = getById(id);        //getById方法查询一个对象，queryById方法查询的是Result类的对象
        //5.判断数据库中是否能查询到该商铺
        if (shop == null) {
            //6.不能查询到：报错：返回404
            return null;
        }

        //8.返回商铺信息
        return shop;
    }

    @Override
    public Result update(Shop shop) {
        Long id = shop.getId();
        if (id == null) {
            Result.fail("不能更新该店铺信息");
        }
        String KEY = CACHE_SHOP_KEY + id.toString();
        //1.先修改数据库
        updateById(shop);
        //2.删除缓存
        stringRedisTemplate.delete(KEY);
        return Result.ok();
    }

    /**
     * 按照距离（或者正常）查询店铺
     *
     * @param typeId  种类名（例如美食，ktv）
     * @param current 当前页
     * @param x       经度（这里写死了）
     * @param y       纬度（这里写死了）
     * @return
     */
    @Override
    public Result queryShopByType(Integer typeId, Integer current, Double x, Double y) {
        //1.判断是否需要按照距离进行查询
        if (x == null || y == null) {
            // 根据类型分页查询
            Page<Shop> page = query()
                    .eq("type_id", typeId)
                    .page(new Page<>(current, SystemConstants.DEFAULT_PAGE_SIZE));
            // 返回数据
            return Result.ok(page.getRecords());
        }
        //计算分页参数
        int from = (current - 1) * SystemConstants.DEFAULT_PAGE_SIZE;
        int end = current * SystemConstants.DEFAULT_PAGE_SIZE;
        //2.根据typeId在redis中获取信息
        String KEY = "shop:geo:" + typeId;
        GeoResults<RedisGeoCommands.GeoLocation<String>> results = stringRedisTemplate.opsForGeo()   // key 圆心 半径 是否带距离
                .search(KEY, GeoReference.fromCoordinate(x, y),
                        new Distance(5000), RedisGeoCommands.GeoSearchCommandArgs.newGeoSearchArgs()
                                .includeDistance().limit(end));
        //3.解析获取的信息(id ,根据id查shop)
        if (results == null) {
            return Result.ok(Collections.emptyList());
        }
        List<GeoResult<RedisGeoCommands.GeoLocation<String>>> list = results.getContent();
        if (list.size() <= from) {
            return Result.ok(Collections.emptyList());
        }
        List<Long> shopIds = new ArrayList<>(list.size());
        HashMap<String, Distance> map = new HashMap<>(list.size());
        list.stream().skip(from).forEach(result -> {
                    String shopIdStr = result.getContent().getName();
                    shopIds.add(Long.valueOf(shopIdStr));
                    @NonNull Distance distance = result.getDistance();
                    map.put(shopIdStr, distance);
                }
        );
        //根据ids查询店铺
        String idStr = StrUtil.join(",", shopIds);
        List<Shop> shops = query().in("id", shopIds).last("ORDER BY FIELD (id ," + idStr + ")").list();
        for (Shop shop : shops) {
            shop.setDistance(map.get(shop.getId().toString()).getValue());
        }
        return Result.ok(shops);
    }






    private Boolean tryLock(Long id) {
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(LOCK_SHOP_KEY + id, "1", LOCK_SHOP_TTL, TimeUnit.SECONDS);
        boolean value = BooleanUtil.isTrue(flag);
        return value;
    }

    private void unLock(Long id) {
        stringRedisTemplate.delete(LOCK_SHOP_KEY + id);
    }
}
